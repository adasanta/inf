import requests
import time
import gspread
from oauth2client.service_account import ServiceAccountCredentials

temps = []
press = []
wind_speeds = []
wind_dirs = []

r = requests.get('https://www.metaweather.com/api/location/551801/')
res = r.json()
temps.append(res['consolidated_weather'][0]['the_temp'])
press.append(res['consolidated_weather'][0]['air_pressure'])
wind_speeds.append(res['consolidated_weather'][0]['wind_speed'])
wind_dirs.append(res['consolidated_weather'][0]['wind_direction'])


r = requests.get('http://api.openweathermap.org/data/2.5/weather?q=Vienna,at&appid=03f16699631fc0d98135e909d4ea7cce')
res = r.json()
temps.append(res['main']['temp']-273.15)
press.append(res['main']['pressure'])
wind_speeds.append(res['wind']['speed'])
wind_dirs.append(res['wind']['deg'])

r = requests.get('http://api.weatherbit.io/v2.0/current?key=fba633e20ff546f2b390f8165204aff7&city=Vienna&country=AT')
res = r.json()
temps.append(res['data'][0]['temp'])
press.append(res['data'][0]['pres'])
wind_dirs.append(res['data'][0]['wind_dir'])
wind_speeds.append(res['data'][0]['wind_spd'])

print(temps)
print(press)
print(wind_dirs)
print(wind_speeds)

scope = ["https://spreadsheets.google.com/feeds"]
creds = ServiceAccountCredentials.from_json_keyfile_name('gd-access.json', scope)
client = gspread.authorize(creds)

sheet = client.open('inf').sheet1
data = sheet.get_all_records()

push_data = []
for i in range(3):
    push_data.append(temps[i])
    push_data.append(press[i])
    push_data.append(wind_speeds[i])
    push_data.append(wind_dirs[i])
    if i==2:
       push_data.append(time.time())

response = sheet.insert_row(push_data, 3)
