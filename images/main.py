import requests
import random
from PIL import Image

query1 = input("Search query 1: ")
query2 = input("Search query 2: ")

r = requests.get('https://pixabay.com/api/?key=7270685-e18ac751549a1c654fce8a9b1&q='+query1)
res = r.json()
r = requests.get(res["hits"][0]["webformatURL"])
i = open('obr_1.jpg', 'wb')
i.write(r.content)
i.close()

r = requests.get('https://pixabay.com/api/?key=7270685-e18ac751549a1c654fce8a9b1&q='+query2)
res = r.json()
r = requests.get(res["hits"][0]["webformatURL"])
i = open('obr_2.jpg', 'wb')
i.write(r.content)
i.close()

im1 = Image.open('obr_1.jpg')
pix1 = im1.load()

im2 = Image.open('obr_2.jpg')
pix2 = im2.load()

method = "random"

if (method == "average"):
    for i in range(min([im1.size[0], im2.size[0]])):
        for j in range(min([im1.size[1], im2.size[1]])):
            pix1[i, j] = ( int((pix1[i, j][0] + pix2[i, j][0])/2), int((pix1[i, j][1] + pix2[i, j][1])/2), int((pix1[i, j][2] + pix2[i, j][2])/2))

if (method == "chess"):
    for i in range(min([im1.size[0], im2.size[0]])):
        for j in range(min([im1.size[1], im2.size[1]])):
            if i % 2 == j % 2:
                pix1[i, j] = ( pix1[i, j][0], pix1[i, j][1], pix1[i, j][2] )
            else:
                pix1[i, j] = ( pix2[i, j][0], pix2[i, j][1], pix2[i, j][2] )

if (method == "random"):
    for i in range(min([im1.size[0], im2.size[0]])):
        for j in range(min([im1.size[1], im2.size[1]])):
            if random.choice([True, False]) == True:
                pix1[i, j] = ( pix1[i, j][0], pix1[i, j][1], pix1[i, j][2] )
            else:
                pix1[i, j] = ( pix2[i, j][0], pix2[i, j][1], pix2[i, j][2] )

im1.save('obr_merge.jpg')

